#!/bin/bash
set -o allexport; source .env; set +o allexport

echo "🥇[4/6]  get the PEM certificate (./config/certificate.txt)"

echo $(kubectl get secret -o go-template='{{index .data "ca.crt" }}' $(kubectl get sa default -o go-template="{{range .secrets}}{{.name}}{{end}}")) | base64 -d > ./config/certificate.txt

cat ./config/certificate.txt

echo "🦊[5/6]  create the GitLab service account"

kubectl apply -f gitlab-service-account.yaml --wait

echo "🔐[6/6] generate the access token (./config/access.token.txt)"

SECRET=$(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')
CLUSTER_SECRET=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 -d) 
echo ${CLUSTER_SECRET}> ./config/access.token.txt
